using System.Collections.Generic;
using System.Linq;

namespace MiniGamesBot.Discord.Games
{
    public enum TicTacToeFieldState
    {
        Free,
        Occupied,
        Winning
    }

    public class TicTacToe
    {
        private const short MaxCombination = 0b111111111;

        private static readonly List<short> WinCombinations = new()
        {
            0b000000111,
            0b000111000,
            0b111000000,
            0b100100100,
            0b010010010,
            0b001001001,
            0b100010001,
            0b001010100
        };

        public Dictionary<bool, int> Fields = new() {{false, 0}, {true, 0}};
        public bool GameActive = true;
        public bool CurrentPlayer;
        public bool? Winner;
        public int WinCombination;

        public bool? GetFieldPlayer(int id)
        {
            return (this.Fields[false] & id) != 0 ? false : (this.Fields[true] & id) != 0 ? true : null;
        }

        public (TicTacToeFieldState state, bool? player) GetFieldState(int id)
        {
            var player = this.GetFieldPlayer(id);
            var state = (this.WinCombination & id) != 0 ? TicTacToeFieldState.Winning :
                player is null ? TicTacToeFieldState.Free : TicTacToeFieldState.Occupied;

            return (state, player);
        }

        public bool Move(int id)
        {
            if (id > MaxCombination) return false;
            if (this.GetFieldPlayer(id) is not null) return false;
            this.Fields[this.CurrentPlayer] |= id;
            
            if (WinCombinations.FirstOrDefault(e => (this.Fields[this.CurrentPlayer] & e) == e) is var winCombination &&
                winCombination != default)
            {
                this.GameActive = false;
                this.Winner = this.CurrentPlayer;
                this.WinCombination = winCombination;
                return true;
            }

            if ((this.Fields[this.CurrentPlayer] | this.Fields[!this.CurrentPlayer]) >= MaxCombination)
            {
                this.GameActive = false;
                this.Winner = null;
                return true;
            }
            
            this.CurrentPlayer = !this.CurrentPlayer;
            return true;
        }

        public void SetWinner(bool? winner)
        {
            this.Winner = winner;
            this.GameActive = false;
        }
    }
}