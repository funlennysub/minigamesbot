using System;
using System.Drawing;
using JetBrains.Annotations;
using MiniGamesBot.Extensions;

namespace MiniGamesBot.Discord.Games.Tetris
{
    // 0 - empty
    // 1 - white
    // 2 - shadow
    // 3 - tetromino 0
    public class Tetromino
    {
        public int Id;
        public int Color => this.Id + 3;

        public Point Position = new(0, 0);

        private int _rotation;

        public int Rotation
        {
            get => this._rotation;
            set
            {
                var val = value;
                while (val < 0) val += 4;
                this._rotation = value % 4;
            }
        }

        public Tetromino(int id)
        {
            this.Id = id;
        }

        public int[,] GetMatrix(bool shadow = false)
        {
            var matrix = TetrisResources.Tetromino[this.Id];
            for (var i = 0; i < this._rotation; i++)
            {
                matrix = matrix.RotateMatrixClockwise();
            }

            return matrix.Apply<int, int>(shadow ? v => v > 0 ? 2 : 0 : v => v > 0 ? this.Color : v);
        }

        public Tetromino Clone()
        {
            return new(this.Id)
            {
                Rotation = this.Rotation,
                Position = this.Position,
                Id = this.Id
            };
        }
    }

    public class Tetris
    {
        public int[,] Field;
        public int Width;
        public int Height;
        public Tetromino CurrentTetromino;
        public Tetromino NextTetromino;
        private Random _random = new();
        public bool GameOver = false;

        public int Points;
        public int Tetromino;
        public int Lines;
        public int Tetrises;

        public Tetris(int width, int height)
        {
            this.Width = width;
            this.Height = height;
            this.Field = new int[height, width];
            this.CurrentTetromino = this.GetRandomTetromino(true);
            this.NextTetromino = this.GetRandomTetromino();
            for (var y = 0; y < height; y++)
            {
                for (var x = 0; x < width; x++)
                {
                    this.Field[y, x] = 0;
                }
            }
        }

        private Tetromino GetRandomTetromino(bool first = false)
        {
            var tetromino = new Tetromino(this._random.Next(0, first ? 5 : 7))
            {
                Rotation = this._random.Next(0, 4)
            };
            tetromino.Position.X = (int) MathF.Floor(this.Width / 2f - tetromino.GetMatrix().GetLength(1) / 2f);
            return tetromino;
        }

        private void BakeTetromino(int[,]? matrix)
        {
            lock (this.CurrentTetromino)
            {
                lock (this.Field)
                {
                    matrix ??= this.CurrentTetromino.GetMatrix();
                    this.Field.Add(matrix, this.CurrentTetromino.Position, (_, v) => v > 0);
                    if (this.DoesCollide(this.NextTetromino.GetMatrix(), this.NextTetromino.Position))
                    {
                        this.GameOver = true;
                        return;
                    }

                    this.Tetromino++;
                    this.CurrentTetromino = this.NextTetromino;
                    this.NextTetromino = this.GetRandomTetromino();
                }
            }
        }

        public bool DoesCollide(int[,] matrix, Point pos)
        {
            var x = pos.X;
            var y = pos.Y;
            var width = matrix.GetLength(1);
            var height = matrix.GetLength(0);

            lock (this.Field)
            {
                for (var row = 0; row < height; row++)
                {
                    for (var column = 0; column < width; column++)
                    {
                        if (matrix[row, column] == 0) continue;
                        if (column + x < 0 || column + x >= this.Width || row + y < 0 || row + y >= this.Height)
                            return true;
                        if (this.Field[row + y, column + x] != 0) return true;
                    }
                }
            }

            return false;
        }

        public int[,] Render()
        {
            lock (this.CurrentTetromino)
            {
                var matrix = this.CurrentTetromino.GetMatrix();
                var pos = this.CurrentTetromino.Position;
                while (!this.DoesCollide(matrix, pos + new Size(0, 1))) pos += new Size(0, 1);
                var field = ((int[,]) this.Field.Clone());
                if (pos != this.CurrentTetromino.Position)
                    field.Add(matrix.Apply(v => v > 0 ? 2 : 0), pos, (_, v) => v > 0);
                field.Add(matrix, this.CurrentTetromino.Position, (_, v) => v > 0);

                return field;
            }
        }

        public void FallDown()
        {
            lock (this.CurrentTetromino)
            {
                var matrix = this.CurrentTetromino.GetMatrix();
                var pos = this.CurrentTetromino.Position;
                while (!this.DoesCollide(matrix, pos + new Size(0, 1))) pos += new Size(0, 1);
                this.CurrentTetromino.Position = pos;
            }
        }

        public void Rotate(bool clockwise)
        {
            lock (this.CurrentTetromino)
            {
                var newTetromino = this.CurrentTetromino.Clone();
                newTetromino.Rotation += clockwise ? 1 : -1;
                var matrix = newTetromino.GetMatrix();
                if (this.DoesCollide(matrix, newTetromino.Position))
                {
                    if (newTetromino.Position.X < 0 || newTetromino.Position.X + matrix.GetLength(1) >= this.Width)
                    {
                        newTetromino.Position += newTetromino.Position.X < 0 ? new Size(1, 0) : new Size(-1, 0);
                        if (this.DoesCollide(matrix, newTetromino.Position)) return;
                    }
                    else return;
                }

                this.CurrentTetromino.Rotation = newTetromino.Rotation;
                this.CurrentTetromino.Position = newTetromino.Position;
            }
        }

        public bool ModCurrentPosition(Size mod)
        {
            lock (this.CurrentTetromino)
            {
                if (this.GameOver) return false;
                var matrix = this.CurrentTetromino.GetMatrix();
                if (this.DoesCollide(matrix, this.CurrentTetromino.Position + mod))
                {
                    if (mod.Height > 0)
                    {
                        if (mod.Width != 0)
                        {
                            var onlyX = this.CurrentTetromino.Position + new Size(mod.Width, 0);
                            if (!this.DoesCollide(matrix, onlyX))
                            {
                                this.CurrentTetromino.Position = onlyX;
                            }
                        }

                        this.BakeTetromino(matrix);
                    }

                    return false;
                }

                this.CurrentTetromino.Position += mod;
            }

            return true;
        }

        public void Tick()
        {
            var didSomething = false;

            lock (this.Field)
            {
                var removed = 0;
                for (var y = 0; y < this.Height; y++)
                {
                    var white = false;
                    var line = true;
                    for (var x = 0; x < this.Width; x++)
                    {
                        if (this.Field[y, x] == 1)
                        {
                            white = true;
                            break;
                        }

                        if (this.Field[y, x] == 0)
                        {
                            line = false;
                            break;
                        }
                    }

                    if (!didSomething) didSomething = white || line;
                    if (white)
                    {
                        removed++;
                        for (var newY = y; newY > 0; newY--)
                        {
                            for (var x = 0; x < this.Width; x++)
                            {
                                this.Field[newY, x] = this.Field[newY - 1, x];
                            }
                        }

                        for (var x = 0; x < this.Width; x++)
                        {
                            this.Field[0, x] = 0;
                        }
                    }
                    else if (line)
                    {
                        for (var x = 0; x < this.Width; x++)
                        {
                            this.Field[y, x] = 1;
                        }
                    }
                }

                if (removed > 0)
                {
                    var points = removed switch
                    {
                        1 => 100,
                        2 => 300,
                        3 => 700,
                        >= 4 => 1500,
                        _ => 0
                    };
                    this.Points += points;
                    this.Lines += removed;
                    if (removed >= 4) this.Tetrises++;
                }
            }

            if (!didSomething)
            {
                this.ModCurrentPosition(new Size(0, 1));
                if (!this.GameOver) this.Points += 5;
            }
        }
    }
}