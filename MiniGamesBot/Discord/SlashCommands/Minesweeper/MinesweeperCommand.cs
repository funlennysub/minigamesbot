using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using DSharpPlus;
using DSharpPlus.Entities;
using DSharpPlus.EventArgs;
using DSharpPlus.SlashCommands;
using JetBrains.Annotations;
using MiniGamesBot.Discord.Games;
using MiniGamesBot.Extensions;

namespace MiniGamesBot.Discord.SlashCommands.Minesweeper
{
    public class MinesweeperCommand : ApplicationCommandModule
    {
        private readonly DiscordClient _discordClient;
        private readonly IMinesweeperEmojiService _minesweeperEmojiService;
        private Games.Minesweeper _game = null!;
        private DiscordUser _player = null!;
        private Point _current = new Point(0, 0);
        private ulong _interactionId = 0;
        private bool _currentMode = true;

        private bool IsActive(int x, int y) => this._game.WonGame == null &&
                                               x >= this._current.X && x < this._current.X + 5 &&
                                               y >= this._current.Y && y < this._current.Y + 4;

        public MinesweeperCommand(DiscordClient discordClient, IMinesweeperEmojiService minesweeperEmojiService)
        {
            this._discordClient = discordClient;
            this._minesweeperEmojiService = minesweeperEmojiService;
        }

        private Task OnInteraction(DiscordClient sender, ComponentInteractionCreateEventArgs interaction)
        {
            if (interaction.Message.Interaction.Id != this._interactionId) return Task.CompletedTask;
            if (this._player.Id != interaction.User.Id)
            {
                interaction.Interaction.CreateResponseAsync(InteractionResponseType.ChannelMessageWithSource,
                    new DiscordInteractionResponseBuilder()
                        .WithContent("Ало, вы шо, ебобо?")
                        .AsEphemeral(true));
                return Task.CompletedTask;
            }

            switch (interaction.Interaction.Data.CustomId)
            {
                case "left":
                    if (this._current.X <= 0) break;
                    this._current.X--;
                    break;
                case "right":
                    if (this._current.X + 5 >= this._game.Width) break;
                    this._current.X++;
                    break;
                case "up":
                    if (this._current.Y <= 0) break;
                    this._current.Y--;
                    break;
                case "down":
                    if (this._current.Y + 4 >= this._game.Width) break;
                    this._current.Y++;
                    break;
                case "currentMode":
                    this._currentMode = !this._currentMode;
                    break;
                default:
                    try
                    {
                        var point = JsonSerializer.Deserialize<Point>(interaction.Interaction.Data.CustomId);
                        var res = this._currentMode
                            ? this._game.Open(point.X, point.Y)
                            : this._game.Flag(point.X, point.Y);
                        if (!res)
                        {
                            interaction.Interaction.CreateResponseAsync(
                                InteractionResponseType.DeferredMessageUpdate);
                            return Task.CompletedTask;
                        }
                    }
                    catch (Exception)
                    {
                        // ignored
                    }

                    break;
            }


            interaction.Interaction.CreateResponseAsync(InteractionResponseType.UpdateMessage,
                new DiscordInteractionResponseBuilder(this.GetMessage().ToMessageBuilder()));

            return Task.CompletedTask;
        }

        private void GameStart()
        {
            this._discordClient.ComponentInteractionCreated += this.OnInteraction;
        }

        private void GameEnd()
        {
            this._discordClient.ComponentInteractionCreated -= this.OnInteraction;
        }

        private DiscordEmoji GetEmoji(int x, int y, bool ignoreActive = false)
        {
            var cell = this._game.Field[y, x];
            var emoji = "m";
            var active = !ignoreActive && this.IsActive(x, y) ? "b" : "a";
            switch (cell.State)
            {
                case MinesweeperCellState.Closed:
                    if (cell.Value < 0 && this._game.WonGame is not null) emoji += "b";
                    else emoji += "c" + active;
                    break;
                case MinesweeperCellState.Flagged:
                    emoji += "f" + (this._game.WonGame is not null && cell.Value >= 0 ? "w" : active);
                    break;
                case MinesweeperCellState.Opened:
                    emoji += cell.Value switch
                    {
                        > 0 and <= 9 => cell.Value + active,
                        < 0 => cell.State == MinesweeperCellState.Opened ? "bb" : "b",
                        0 => "o" + active,
                        _ => ""
                    };
                    break;
            }

            return this._minesweeperEmojiService.GetEmoji(emoji);
        }

        private string FormatEmoji(DiscordEmoji emoji)
        {
            return $"<:_:{emoji.Id}>";
        }

        private DiscordWebhookBuilder GetMessage()
        {
            var message = new DiscordWebhookBuilder();
            var field = new StringBuilder();
            for (var y = 0; y < this._game.Field.GetLength(0); y++)
            {
                for (var x = 0; x < this._game.Field.GetLength(1); x++)
                {
                    field.Append(this.FormatEmoji(this.GetEmoji(x, y)));
                }

                field.Append('\n');
            }

            if (this._game.WonGame is null)
            {
                for (var y = this._current.Y; y < this._current.Y + 4; y++)
                {
                    var buttons = new List<DiscordButtonComponent>();
                    for (var x = this._current.X; x < this._current.X + 5; x++)
                    {
                        if (x + 1 > this._game.Width || y + 1 > this._game.Height)
                        {
                            buttons.Add(
                                new DiscordButtonComponent(
                                    ButtonStyle.Secondary,
                                    JsonSerializer.Serialize(new Point(x, y)),
                                    "\u200b",
                                    true,
                                    new DiscordComponentEmoji(this._minesweeperEmojiService.GetEmoji("mca"))
                                )
                            );
                            continue;
                        }

                        var cell = this._game.Field[y, x];
                        buttons.Add(
                            new DiscordButtonComponent(
                                ButtonStyle.Secondary,
                                JsonSerializer.Serialize(new Point(x, y)),
                                "\u200b",
                                this._currentMode ? !this._game.CanBeOpened(x, y) : !this._game.CanBeFlagged(x, y),
                                new DiscordComponentEmoji(this.GetEmoji(x, y, true))
                            )
                        );
                    }

                    message.AddComponents(buttons);
                }

                message.AddComponents(
                    new DiscordButtonComponent(ButtonStyle.Primary, "left", "\u200b",
                        this._current.X <= 0,
                        new DiscordComponentEmoji(DiscordEmoji.FromUnicode("⬅️"))),
                    new DiscordButtonComponent(ButtonStyle.Primary, "right", "\u200b",
                        this._current.X + 5 >= this._game.Width,
                        new DiscordComponentEmoji(DiscordEmoji.FromUnicode("➡️"))),
                    new DiscordButtonComponent(ButtonStyle.Primary, "up", "\u200b",
                        this._current.Y <= 0,
                        new DiscordComponentEmoji(DiscordEmoji.FromUnicode("⬆️"))),
                    new DiscordButtonComponent(ButtonStyle.Primary, "down", "\u200b",
                        this._current.Y + 4 >= this._game.Height,
                        new DiscordComponentEmoji(DiscordEmoji.FromUnicode("⬇️"))),
                    new DiscordButtonComponent(ButtonStyle.Primary, "currentMode", "Текущий режим",
                        false,
                        new DiscordComponentEmoji(DiscordEmoji.FromUnicode(this._currentMode ? "⛏️" : "🚩")))
                );
            }
            else
            {
                this.GameEnd();
                message.AddEmbed(new DiscordEmbedBuilder()
                    .WithDescription(this._game.WonGame == true ? "Вы выиграли!" : "Вы проиграли!"));
            }

            return message.WithContent(field.ToString());
        }

        [UsedImplicitly]
        [SlashCommand("minesweeper", "Игра в сапёра")]
        public async Task Command(InteractionContext ctx, [Option("fieldWidth", "Ширина поля")] long? fieldWidth = null,
            [Option("fieldHeight", "Высота поля")] long? fieldHeight = null, [Option("bombs", "Количество мин")] long? bombs = null)
        {
            await this.Game(ctx, fieldWidth, fieldHeight, bombs);
        }

        private async Task Game(BaseContext ctx, long? fieldWidth, long? fieldHeight, long? bombs)
        {
            fieldWidth ??= fieldHeight ?? 8;
            fieldHeight ??= fieldWidth;
            bombs ??= (int) (fieldWidth + fieldHeight) / 2;

            string? error = null;
            var cells = (int) (fieldWidth * fieldHeight);
            var maxBombs = cells - 9;
            if (fieldWidth < 5) error = "Минимальная ширина поля - 5 клеток";
            else if (fieldWidth > 9) error = "Максимальная ширина поля - 8 клеток";
            else if (fieldHeight < 5) error = "Минимальная высота поля - 5 клеток";
            else if (fieldHeight > 9) error = "Максимальная высота поля - 8 клеток";
            else if (bombs < 1) error = "Нужна хотя бы одна бомба";
            else if (bombs > maxBombs) error = $"Максимальное количество бомб на поле {fieldWidth}x{fieldHeight} - {maxBombs}";


            if (error is not null)
            {
                await ctx.CreateResponseAsync(InteractionResponseType.ChannelMessageWithSource,
                    new DiscordInteractionResponseBuilder()
                        .WithContent(error)
                        .AsEphemeral(true));
                return;
            }

            this._interactionId = ctx.Interaction.Id;
            await this._minesweeperEmojiService.UpdateEmojis(ctx.Client);
            this._player = ctx.User;
            this._game = new Games.Minesweeper((int) fieldWidth, (int) fieldHeight, (int) bombs);
            this.GameStart();
            await ctx.CreateResponseAsync(InteractionResponseType.ChannelMessageWithSource,
                new DiscordInteractionResponseBuilder(this.GetMessage().ToMessageBuilder()));
        }
    }
}