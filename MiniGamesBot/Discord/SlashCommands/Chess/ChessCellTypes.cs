using System;

namespace MiniGamesBot.Discord.SlashCommands.Chess
{
    // 0 000 - black on black
    // 1 001 - black on white
    // 2 010 - white on black
    // 3 011 - white on white
    // 4 100 - black on black active
    // 5 101 - black on white active
    // 6 110 - white on black active
    // 7 111 - white on white active
    
    [Flags]
    public enum ChessCellTypes
    {
        WhiteCell = 0b001,
        WhitePiece = 0b010,
        Active = 0b100
    }
}