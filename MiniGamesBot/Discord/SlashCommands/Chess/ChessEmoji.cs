using System.Drawing;
using MiniGamesBot.Discord.Games;

namespace MiniGamesBot.Discord.SlashCommands.Chess
{
    public readonly struct ChessEmoji
    {
        public PieceType PieceType { get; }
        public bool PieceColor { get; }
        public bool CellColor { get; }
        public bool Active { get; }

        public ChessEmoji(PieceType pieceType, bool pieceColor, bool cellColor, bool active)
        {
            this.PieceType = pieceType;
            this.PieceColor = pieceColor;
            this.CellColor = cellColor;
            this.Active = active;
        }

        public ChessEmoji(PieceType pieceType, ChessCellTypes chessCellType)
        {
            this.PieceType = pieceType;
            this.PieceColor = chessCellType.HasFlag(ChessCellTypes.WhitePiece);
            this.CellColor = chessCellType.HasFlag(ChessCellTypes.WhiteCell);
            this.Active = chessCellType.HasFlag(ChessCellTypes.Active);
        }

        public ChessEmoji(Piece? piece, Point pos, bool active)
        {
            var white = (pos.X + pos.Y) % 2 == 0;
            this.PieceType = piece?.Type ?? PieceType.Empty;
            this.PieceColor = piece?.Color ?? false;
            this.CellColor = white;
            this.Active = active;
        }
    }
}