using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DSharpPlus;
using DSharpPlus.Entities;
using MiniGamesBot.Discord.Games;

namespace MiniGamesBot.Discord.SlashCommands.Chess
{
    public interface IChessEmojisService
    {
        Task UpdateEmojis(BaseDiscordClient client);
        DiscordEmoji GetEmoji(ChessEmoji emoji);
    }

    public class ChessEmojisService : IChessEmojisService
    {
        private readonly Dictionary<ChessEmoji, DiscordEmoji> _emojis = new();

        private static readonly List<ulong> EmojiGuilds = new()
        {
            860818942800297995
        };

        private static readonly Dictionary<char, PieceType> PieceLetters = new()
        {
            {'b', PieceType.Bischop},
            {'k', PieceType.King},
            {'n', PieceType.Knight},
            {'p', PieceType.Pawn},
            {'q', PieceType.Queen},
            {'r', PieceType.Rook},
            {'_', PieceType.Empty}
        };

        public async Task UpdateEmojis(BaseDiscordClient client)
        {
            if (this._emojis.Count > 0) return;
            foreach (var discordGuild in client.Guilds.Values.Where(g => EmojiGuilds.Contains(g.Id)))
            {
                foreach (var emoji in await discordGuild.GetEmojisAsync())
                {
                    if (emoji.Name.Length != 2) continue;
                    if (!byte.TryParse(emoji.Name[0].ToString(), out var cellType) || cellType > 0b111) continue;
                    if (!PieceLetters.TryGetValue(emoji.Name[1], out var pieceType)) continue;
                    this._emojis.Add(new ChessEmoji(pieceType, (ChessCellTypes) cellType), emoji);
                }
            }

            this._emojis.Add(new ChessEmoji(PieceType.Empty, false, true, false), DiscordEmoji.FromUnicode("⬜"));
            this._emojis.Add(new ChessEmoji(PieceType.Empty, false, false, false), DiscordEmoji.FromUnicode("⬛"));
        }

        public DiscordEmoji GetEmoji(ChessEmoji emoji)
        {
            return this._emojis[emoji];
        }
    }
}