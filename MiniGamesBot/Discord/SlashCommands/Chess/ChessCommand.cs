using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using DSharpPlus;
using DSharpPlus.Entities;
using DSharpPlus.EventArgs;
using DSharpPlus.SlashCommands;
using JetBrains.Annotations;
using MiniGamesBot.Discord.Games;
using MiniGamesBot.Discord.Services;
using MiniGamesBot.Extensions;

namespace MiniGamesBot.Discord.SlashCommands.Chess
{
    [UsedImplicitly]
    public class ChessCommand : ApplicationCommandModule
    {
        private readonly IChessEmojisService _emojis;
        private readonly DiscordClient _client;
        private readonly IQuestionService _questionService;

        public ChessCommand(IChessEmojisService emojis, DiscordClient client,
            IQuestionService questionService)
        {
            this._emojis = emojis;
            this._client = client;
            this._questionService = questionService;
        }

        private static readonly Dictionary<PieceType, string> PieceNames = new()
        {
            {PieceType.Bischop, "Слон"},
            {PieceType.King, "Король"},
            {PieceType.Queen, "Ферзь"},
            {PieceType.Knight, "Конь"},
            {PieceType.Rook, "Ладья"},
            {PieceType.Pawn, "Пешка"},
            {PieceType.Empty, ""}
        };

        private static readonly List<PieceType> TransformPieces = new()
        {
            PieceType.Bischop,
            PieceType.Queen,
            PieceType.Knight,
            PieceType.Rook
        };

        private Dictionary<bool, DiscordUser> _players;
        private Games.Chess _game;
        private int? _selectedTarget;
        private ulong? _interactionId;

        private static string GetCellName(Point point)
        {
            return $"{char.ConvertFromUtf32(point.X + 'A')}{8 - point.Y}";
        }


        private static bool GetCellColor(Point point)
        {
            return (point.X + point.Y) % 2 == 0;
        }

        private string DrawField(ICollection<Point> activePieces)
        {
            var str = new StringBuilder();

            const string letters = "#️⃣🇦​🇧​🇨​🇩​🇪​🇫​🇬​🇭#️⃣";
            string[] numbers = {"8️⃣", "7️⃣", "6️⃣", "5️⃣", "4️⃣", "3️⃣", "2️⃣", "1️⃣"};
            str.Append(letters + '\n');

            for (var y = 0; y < 8; y++)
            {
                str.Append(numbers[y] + "​");
                for (var x = 0; x < 8; x++)
                {
                    var point = new Point(x, y);
                    var white = GetCellColor(point);
                    var emoji = this._emojis.GetEmoji(this._game.Pieces.TryGetValue(point, out var piece)
                        ? new ChessEmoji(piece.Type, piece.Color, white, activePieces.Contains(point))
                        : new ChessEmoji(PieceType.Empty, false, white, activePieces.Contains(point)));
                    str.Append(emoji);
                }

                str.Append("​" + numbers[y] + "\n");
            }

            str.Append(letters);

            return str.ToString();
        }

        private DiscordWebhookBuilder GetMessage()
        {
            var available = this._game.AvailableMoves.Select(e => e.Value.Point);
            if (this._game.SelectedPoint is { } point) available = available.Append(point);

            var builder = new DiscordWebhookBuilder()
                .WithContent(this.DrawField(available.ToList()));

            // var (white, black) = this._game.GetMaterialPrices();
            var description =
                $"Белые: {this._players[true].Mention}\nЧерные: {this._players[false].Mention}\n\n";

            switch (this._game.GameState)
            {
                case GameState.Active:
                    description += this._game.CurrentPlayer ? "Ход белых!" : "Ход чёрных!";

                    if (this._game.IsCheck)
                    {
                        description += " Шах!";
                    }

                    builder.AddSelect("source", "Фигура", "Нет доступных фигур", this._game.AvailablePieces
                        .OrderBy(p => p.Key.Y).ThenBy(p => p.Key.X)
                        .Select(e =>
                            new DiscordSelectComponentOption(
                                GetCellName(e.Key),
                                JsonSerializer.Serialize(e.Key),
                                PieceNames[e.Value.Type],
                                this._game.SelectedPoint == e.Key,
                                new DiscordComponentEmoji(this._emojis.GetEmoji(new ChessEmoji(e.Value, e.Key, false)))
                            )
                        ).ToList());

                    builder.AddSelect("target", "Ход", "Нет доступных ходов", this._game.AvailableMoves
                        .Select(e =>
                        {
                            var pieceExists = this._game.Pieces.TryGetValue(e.Value.Point, out var piece);
                            return new DiscordSelectComponentOption(
                                e.Value.Name,
                                e.Key.ToString(),
                                pieceExists ? PieceNames[piece!.Type] : null,
                                this._selectedTarget == e.Key,
                                new DiscordComponentEmoji(
                                    this._emojis.GetEmoji(new ChessEmoji(piece, e.Value.Point, false)))
                            );
                        }).ToList());

                    builder.AddComponents(
                        new DiscordButtonComponent(ButtonStyle.Primary, "confirm", "Подтвердить",
                            this._selectedTarget is null),
                        new DiscordButtonComponent(ButtonStyle.Secondary, "offerTie", "Предложить ничью"),
                        new DiscordButtonComponent(ButtonStyle.Danger, "giveUp", "Сдаться")
                    );

                    break;

                case GameState.PendingTransformation:

                    builder.AddSelect("transform", "Фигура для трансформации", "Нет доступных фигур", TransformPieces
                        .Select(e =>
                            new DiscordSelectComponentOption(
                                PieceNames[e],
                                ((int) e).ToString(),
                                null,
                                false,
                                new DiscordComponentEmoji(
                                    this._emojis.GetEmoji(new ChessEmoji(e, this._game.CurrentPlayer, true, false)))
                            )
                        ).ToList());
                    break;

                case GameState.TieRequest:
                    description += $"{(!this._game.CurrentPlayer ? "Белые" : "Чёрные")} предложили ничью!";
                    builder.AddComponents(
                        new DiscordButtonComponent(ButtonStyle.Primary, "tieConfirm", "Принять"),
                        new DiscordButtonComponent(ButtonStyle.Danger, "tieCancel", "Отклонить")
                    );
                    break;
                case GameState.Ended:
                    description += this._game.Winner is not { } winner ? "Ничья!" :
                        winner ? "Победа белых!" : "Победа чёрных!";
                    this.GameEnd();
                    break;
                default:
                    break;
            }


            builder.AddEmbed(new DiscordEmbedBuilder()
                .WithDescription(description));


            return builder;
        }

        private async Task OnInteraction(DiscordClient sender, ComponentInteractionCreateEventArgs interaction)
        {
            if (interaction.Message.Interaction.Id != this._interactionId) return;

            if (interaction.User.Id != this._players[this._game.CurrentPlayer].Id)
            {
                await interaction.Interaction.CreateResponseAsync(
                    InteractionResponseType.ChannelMessageWithSource,
                    new DiscordInteractionResponseBuilder()
                        .WithContent("ТЫ СЛАВА МЕРЛОУ?")
                        .AsEphemeral(true)
                );
                return;
            }

            switch (interaction.Interaction.Data.CustomId)
            {
                case "source":
                    this._selectedTarget = null;

                    try
                    {
                        this._game.SetSelectedPoint(JsonSerializer.Deserialize<Point>(interaction.Values[0] ?? ""));
                    }
                    catch (Exception)
                    {
                        this._game.SetSelectedPoint(null);
                    }

                    break;

                case "target":
                    if (int.TryParse(interaction.Values[0] ?? "", out var target) && target >= 0)
                    {
                        this._selectedTarget = target;
                    }
                    else
                    {
                        this._selectedTarget = null;
                        this._game.SetSelectedPoint(null);
                    }

                    break;

                case "transform":
                    if (!int.TryParse(interaction.Values[0] ?? "", out var piece) ||
                        !this._game.Transform((PieceType) piece))
                    {
                        await interaction.Interaction.CreateResponseAsync(
                            InteractionResponseType.ChannelMessageWithSource,
                            new DiscordInteractionResponseBuilder()
                                .WithContent("Неверный ход!")
                                .AsEphemeral(true)
                        );
                    }

                    break;

                case "confirm":
                    if (this._game.SelectedPoint is null || this._selectedTarget is null ||
                        !this._game.MovePiece((int) this._selectedTarget!))
                    {
                        await interaction.Interaction.CreateResponseAsync(
                            InteractionResponseType.ChannelMessageWithSource,
                            new DiscordInteractionResponseBuilder()
                                .WithContent("Неверный ход!")
                                .AsEphemeral(true)
                        );
                        break;
                    }

                    this._selectedTarget = null;

                    break;

                case "giveUp":
                    this._game.SetWinner(!this._game.CurrentPlayer);

                    break;

                case "offerTie":
                    this._game.OfferTie();

                    break;

                case "tieConfirm":
                    this._game.AnswerTieOffer(true);

                    break;

                case "tieCancel":
                    this._game.AnswerTieOffer(false);

                    break;
            }

            if (interaction.Handled) return;
            var msg = this.GetMessage();
            await interaction.Interaction.CreateResponseAsync(InteractionResponseType.UpdateMessage,
                new DiscordInteractionResponseBuilder(msg.ToMessageBuilder()));
        }

        private void GameEnd()
        {
            this._client.ComponentInteractionCreated -= this.OnInteraction;
        }

        private void GameStart()
        {
            this._client.ComponentInteractionCreated += this.OnInteraction;
        }

        [UsedImplicitly]
        [ContextMenu(ApplicationCommandType.UserContextMenu, "Шахматы")]
        public async Task ContextMenu(ContextMenuContext ctx)
        {
            await this.Game(ctx, ctx.TargetUser);
        }

        [UsedImplicitly]
        [SlashCommand("chess", "Игра Шахматы")]
        public async Task Command(InteractionContext ctx, [Option("opponent", "Ваш противник")] DiscordUser opponent)
        {
            await this.Game(ctx, opponent);
        }

        public async Task Game(BaseContext ctx, DiscordUser opponent)
        {
            await this._emojis.UpdateEmojis(this._client);
            this._interactionId = ctx.Interaction.Id;

            if (opponent.Id == ctx.Client.CurrentUser.Id)
                opponent = ctx.User;

            if (opponent.IsBot)
                await ctx.CreateResponseAsync(InteractionResponseType.ChannelMessageWithSource,
                    new DiscordInteractionResponseBuilder()
                        .WithContent("С ботом играть нельзя. Если не с кем играть - играй сам с собой")
                        .AsEphemeral(true));

            var res = await this._questionService.GameStartQuestion(ctx,
                opponent.Id != ctx.User.Id
                    ? $"{opponent.Mention}, хочешь поиграть в шахматы с {ctx.User.Mention}?"
                    : $"{opponent.Mention}, хочешь поиграть сам с собой?", opponent);
            if (!res)
            {
                this.GameEnd();
                return;
            }

            this._game = new Games.Chess();
            this._game.Reset();

            this._players = new Dictionary<bool, DiscordUser>
            {
                {true, ctx.User!},
                {false, opponent}
            };
            this.GameStart();

            await ctx.EditResponseAsync(this.GetMessage());
        }
    }
}