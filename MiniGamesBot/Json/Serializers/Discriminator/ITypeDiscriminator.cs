﻿namespace MiniGamesBot.Json.Serializers.Discriminator
{
    public interface ITypeDiscriminator
    {
        public string TypeDiscriminator { get; }
    }
}