﻿using System;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Text.Json;
using System.Threading.Tasks;
using DSharpPlus;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.VisualBasic.CompilerServices;
using MiniGamesBot.Discord.Games;
using MiniGamesBot.Discord.Games.Tetris;
using MiniGamesBot.Discord.Services;
using MiniGamesBot.Discord.SlashCommands.Chess;
using MiniGamesBot.Discord.SlashCommands.Minesweeper;
using MiniGamesBot.Extensions;

namespace MiniGamesBot
{
    internal static class Program
    {
        private static void Main()
        {
            Run().GetAwaiter().GetResult();
        }

        private static void Display(Tetris tetris)
        {
            var a = tetris.Render();

            Console.WriteLine();
            for (var row = 0; row < a.GetLength(0); row++)
            {
                for (var column = 0; column < a.GetLength(1); column++)
                {
                    var value = a[row, column];
                    Console.Write((value == 0 ? " " : value) + " ");
                }

                Console.WriteLine();
            }

        }

        private static async Task Run()
        {
            // var tetris = new Tetris(5, 10);
            // Task.Run(() =>
            // {
            //     while (Console.KeyAvailable)
            //     {
            //         var key = Console.ReadKey(true);
            //         switch (key.KeyChar)
            //         {
            //             case 'A':
            //                 tetris.ModCurrentPosition(new Size(-1, 0));
            //                 break;
            //             case 'D':
            //                 tetris.ModCurrentPosition(new Size(1, 0));
            //                 break;
            //         }
            //     }
            //
            // });
            // while(true)
            // {
            //     Console.Clear();
            //     Display(tetris);
            //     tetris.Tick();
            //     if (tetris.GameOver) break;
            //     await Task.Delay(500);
            // }
            // Console.WriteLine("Game over!");
            var config = new Config();
            var discordConfig = new DiscordConfiguration
            {
                Token = config.Entries.Token,
                TokenType = TokenType.Bot,
                AutoReconnect = true,
                MinimumLogLevel = LogLevel.Information,
                Intents = DiscordIntents.Guilds | DiscordIntents.GuildMembers |
                          DiscordIntents.GuildIntegrations | DiscordIntents.GuildMessages |
                          DiscordIntents.GuildPresences | DiscordIntents.GuildMessageReactions
            };
            
            var discordClient = new DiscordClient(discordConfig);
            discordClient.ClientErrored += (_, args) =>
            {
                Console.WriteLine(args.Exception.ToString());
                return Task.CompletedTask;
            };
            var serviceProvider = new ServiceCollection()
                .AddSingleton(discordClient)
                .AddSingleton<IConfig>(config)
                .AddSingleton<IComponentInteractionCollector, ComponentInteractionCollector>()
                .AddSingleton<IChessEmojisService, ChessEmojisService>()
                .AddSingleton<IMinesweeperEmojiService, MinesweeperEmojiService>()
                .AddSingleton<IQuestionService, QuestionService>()
                .BuildServiceProvider(true);
            
            serviceProvider.ResolveCommands();
            
            await discordClient.ConnectAsync();
            await Task.Delay(-1);
        }
    }
}